package ru.t1.nkiryukhin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull List<M> findAll(@NotNull Comparator<M> comparator);

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id) throws AbstractFieldException;

    @NotNull
    M findOneByIndex(@NotNull Integer index) throws AbstractFieldException;

    int getSize();

    @Nullable
    M removeOne(@Nullable M model) throws UserIdEmptyException;

    @Nullable
    M removeById(@NotNull String id) throws AbstractFieldException;

    @Nullable
    M removeByIndex(@NotNull Integer index) throws AbstractFieldException;

    void removeAll(@NotNull Collection<M> collection);

}