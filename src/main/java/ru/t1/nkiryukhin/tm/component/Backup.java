package ru.t1.nkiryukhin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.command.data.AbstractDataCommand;
import ru.t1.nkiryukhin.tm.command.data.DataBackupLoadCommand;
import ru.t1.nkiryukhin.tm.command.data.DataBackupSaveCommand;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() throws AbstractException {
        load();
        start();
    }

    public void save() throws AbstractException {
        bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() throws AbstractException {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.processCommand(DataBackupLoadCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            save();
        }
    }

}
