package ru.t1.nkiryukhin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.nkiryukhin.tm.api.repository.ICommandRepository;
import ru.t1.nkiryukhin.tm.api.repository.IProjectRepository;
import ru.t1.nkiryukhin.tm.api.repository.ITaskRepository;
import ru.t1.nkiryukhin.tm.api.repository.IUserRepository;
import ru.t1.nkiryukhin.tm.api.service.*;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.nkiryukhin.tm.exception.system.CommandNotSupportedException;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.repository.CommandRepository;
import ru.t1.nkiryukhin.tm.repository.ProjectRepository;
import ru.t1.nkiryukhin.tm.repository.TaskRepository;
import ru.t1.nkiryukhin.tm.repository.UserRepository;
import ru.t1.nkiryukhin.tm.service.*;
import ru.t1.nkiryukhin.tm.util.SystemUtil;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;


public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.nkiryukhin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void initBackup() throws AbstractException {
        backup.init();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() throws AbstractException {
        @NotNull final User user = userService.create("user", "123", "user1@mail.com");
        @NotNull final User nkiryukhin = userService.create("nkiryukhin", "666", "na@mail.com");
        @NotNull final User admin = userService.create("admin", "sadmin", Role.ADMIN);

        projectService.add(user.getId(), new Project("proj2", "project two", Status.IN_PROGRESS));
        projectService.add(nkiryukhin.getId(), new Project("proj1", "project one", Status.COMPLETED));
        projectService.add(nkiryukhin.getId(), new Project("proj4", "project four", Status.NOT_STARTED));
        projectService.add(admin.getId(), new Project("proj3", "project three", Status.IN_PROGRESS));

        taskService.create(user.getId(), "t2", "task two", Status.IN_PROGRESS);
        taskService.create(nkiryukhin.getId(), "t1", "task one", Status.COMPLETED);
        taskService.create(nkiryukhin.getId(), "t4", "task four", Status.NOT_STARTED);
        taskService.create(admin.getId(), "t3", "task three", Status.IN_PROGRESS);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    void processCommand(@Nullable final String command, boolean checkRoles) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) throws AbstractException {
        processCommand(command, true);
    }

    private boolean processArguments(@Nullable final String[] args) throws AbstractException {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(@Nullable final String argument) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        try {
            initPID();
            initLogger();
            initDemoData();
            initBackup();
            processArguments(args);
        } catch (@NotNull final AbstractException e) {
            loggerService.error(e);
        }

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}