package ru.t1.nkiryukhin.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.model.ICommand;
import ru.t1.nkiryukhin.tm.api.service.IAuthService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.api.service.IServiceLocator;
import ru.t1.nkiryukhin.tm.api.service.IUserService;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract Role[] getRoles();

    @NotNull
    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    public String getUserId() throws AccessDeniedException {
        return getAuthService().getUserId();
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getName();
        @Nullable final String argument = getArgument();
        @NotNull final String description = getDescription();
        @NotNull String result = "";
        if (!name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (!description.isEmpty()) result += description;
        return result;
    }

}
