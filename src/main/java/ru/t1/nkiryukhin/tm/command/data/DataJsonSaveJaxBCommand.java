package ru.t1.nkiryukhin.tm.command.data;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.Domain;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in json file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-json-jaxb";
    }

    @SneakyThrows
    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE JSON]");
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);
        @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
