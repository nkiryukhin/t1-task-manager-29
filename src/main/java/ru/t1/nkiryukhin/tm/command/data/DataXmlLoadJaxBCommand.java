package ru.t1.nkiryukhin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.dto.Domain;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from xml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-jaxb";
    }

    @SneakyThrows
    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD XML]");
        @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final File file = new File(FILE_XML);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    public @NotNull Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
